# Traveling Salesman Problen

## Intro
A python implementation of a solution to the travelling salesman problem using a genetic algorithm.

## Usage

Developed on python 2.7

Simply run "python TSM.py"

There are a bunch of controls at the top of TSM.py to set number of towns; iterations etc.