#
# Author: Lawrence Mitchell
# Date: 9-Nov-2019
# Title: Solving the travelling salesmen problem with a genetic algorithm
#
# Description:
# A program to solve the travelling salesmen problem. 
# Solved with two methods:
#   1) for reference we randomly generate routes and capture the shortest one we found.
#   2) A genetic algorithm solves the problem. 
# Both methods effectively get the same number of tries, the GA usually solves the problem with a much shorter final distance.
#

from graphics import *
from Town import Town
import random
import route as rt
import time
import copy

#Setup of the graphics canvas(es)
mapWidth = 500
mapHeight = 500

#Setup of the map
townNum = 20    #Number of towns the sailsman must travel to.

#Genetic algo config
popSize = 20            #Number of salesmen per iteration.
iterations = 1000       #Number of iterations.
genRate = 20            #Percentage which make it to the next generation.
mutateRate = 100.0      #Percentage which are mutated of the new generation.

def main():
  
  #Prepare towns.
  towns = []
  for i in range(townNum):
    towns.append ( Town( random.randrange(mapWidth),  random.randrange(mapHeight)))
    
  #################################### Random method first #######################################################
  winRand = GraphWin("Travelling Sales Man (Rand)", mapWidth, mapHeight)
  for town in towns:
    town.draw(winRand)
    
  
  shortVal = 1000000                     #A big number to init, hopfully bigger than any solution length.
  for i in range(popSize*iterations):   
    route = rt.Route(townNum)            #Make a new random route.
    len = route.len(towns, townNum)      #Find out the length of this new route.
    #Everytime a new route is found to be the shortest, log it.
    if len < shortVal:                   
      shortVal = len
      best = route
  #We have our shortest route so draw on the canvas what we've found.
  drawLines(winRand, townNum, towns, best)
  #Print the shortest distance on the canvas.
  t_res_rand = "Short: %i" % shortVal
  t = Text(Point(mapWidth/2,mapHeight/2), t_res_rand)
  t.setTextColor("Blue")
  t.draw(winRand)


  ################## Now the genetic algorithm ###########################################
  winGA = GraphWin("Travelling Sales Man (Genetic)", mapWidth, mapHeight)
  for town in towns:
    town.draw(winGA)
  
  #Start with some new routes.
  routes = []
  for i in range(popSize):   
    route = rt.Route(townNum)
    routes.append(route)
    
  for iter in range (iterations):
    #Route_list stores routes and length. Generate this from the routes we have.
    route_list = []   
    for route in routes:
      route_list.append((route, route.len(towns, townNum)))
    route_list.sort(key=lambda routes: routes[1])  #Order the routes on length; short to long.

    #Slice out the survivors of this run for the next generation.
    slice_num = int((genRate * popSize)/100)
    routes = []
    for i in range(slice_num):
      routes.append(copy.deepcopy(route_list[i][0]))

    #Zip through the remaining route slots (after killing the slow routes) 
    #  and copy in the survivors but with some random mutation.
    copy_sel = slice_num -1
    for i in range(popSize-slice_num):
      mutated = copy.copy(route_list[copy_sel][0])
      mutated.mutate(mutateRate/100, townNum)
      routes.append(copy.deepcopy(mutated))
      copy_sel = copy_sel - 1
      if copy_sel < 0:
        copy_sel = slice_num -1
  
  #We've gone through all the iterations; we now have to create a final route_list, 
  #   and order it so we can pick the winner.
  route_list = []   
  for route in routes:
    route_list.append((route, route.len(towns, townNum)))
  route_list.sort(key=lambda routes: routes[1])  #Order the routes.  
  drawLines(winGA, townNum, towns, route_list[0][0])
  t_res_ga = "Short: %i" % route_list[0][1]
  t = Text(Point(mapWidth/2,mapHeight/2), t_res_ga)
  t.setTextColor("Blue")
  t.draw(winGA)
  
  print("Shortest random found : %i",shortVal)
  print("Shortest GA found : %i", route_list[0][1])
  #Close the windows off after an entry
  raw_input("Hit any key to close")
  winRand.close()
  winGA.close()

def drawLines(window, townNum, towns, route):
  lines = []
  for i in range(1, townNum):
    lines.append( Line(towns[route.order[i-1]].getPoint(), towns[route.order[i]].getPoint()) )
    
  for line in lines:
    line.draw(window)
main()