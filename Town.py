from graphics import *

# A class to describe a "town". 
class Town():
  #Position.
  xPos = 0;
  yPos = 0;
  #How thick are the towns to be drawn/
  townWidth = 10
  
  def __init__(self, x, y):
    self.xPos = x
    self.yPos = y
    
  def draw(self, win):
    c = Circle(Point(self.xPos, self.yPos), self.townWidth)
    c.setFill('Black')
    c.draw(win)
    
  def getPoint(self):
    return Point(self.xPos, self.yPos)