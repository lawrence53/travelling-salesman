import numpy as np
import random

#A class to describe a route.
#The route is simply an array of numbers of length same number as the number of towns in the problem to be solved.
class Route():
  def __init__(self, townNum):
    #Get the route order and shuffle it.
    self.order = np.arange(townNum)  
    np.random.shuffle(self.order)
    return
    
  #Work out the length of this route.
  def len(self, towns, townNum):
    len = 0
    for i in range(townNum-1):
      xs = np.square(towns[self.order[i]].xPos - towns[self.order[i+1]].xPos)
      ys = np.square(towns[self.order[i]].yPos - towns[self.order[i+1]].yPos)
      len = len + np.sqrt(xs + ys)
    return int(len)
  
  #This procedure introduces a random single shuffle, this is how we genetically mutate.
  #The probability 0.000 to 1.000 defines the probability of if this route will be mutated.
  def mutate(self, probability, townNum):
    if random.random() < probability:
      swap1 =  random.randint(0, townNum-1)
      swap2 =  random.randint(0, townNum-1)
      self.order[swap1], self.order[swap2] = self.order[swap2], self.order[swap1]
    